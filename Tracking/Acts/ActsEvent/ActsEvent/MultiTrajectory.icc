/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

//  has_impl implementation
namespace ActsTrk {
namespace details {
constexpr std::optional<bool> has_impl(
    const xAOD::TrackStateContainer& trackStates, Acts::HashedString key,
    ActsTrk::IndexType istate) {
  using namespace Acts::HashedStringLiteral;
  using Acts::MultiTrajectoryTraits::kInvalid;
  switch (key) {
    case "previous"_hash:
      return trackStates[istate]->previous() < kInvalid;
    case "chi2"_hash:
      return trackStates[istate]->chi2() < kInvalid;
    case "pathLength"_hash:
      return trackStates[istate]->pathLength() < kInvalid;
    case "typeFlags"_hash:
      return static_cast<IndexType>(trackStates[istate]->typeFlags()) < kInvalid;
    case "predicted"_hash:
      return trackStates[istate]->predicted() < kInvalid;
    case "filtered"_hash:
      return trackStates[istate]->filtered() < kInvalid;
    case "smoothed"_hash:
      return trackStates[istate]->smoothed() < kInvalid;
    case "jacobian"_hash:
      return trackStates[istate]->jacobian() < kInvalid;
    case "projector"_hash:
      return trackStates[istate]->calibrated() < kInvalid;
    case "calibrated"_hash:
      return trackStates[istate]->calibrated() < kInvalid;
    case "calibratedCov"_hash:
      return trackStates[istate]->calibrated() < kInvalid;
    case "measdim"_hash:
      return trackStates[istate]->measDim() < kInvalid;
      // TODO restore once only the EL Source Links are in use 
      // return !trackStates[istate]->uncalibratedMeasurementLink().isDefault();
  }
  return std::optional<bool>();
}
}  // namespace details
}  // namespace ActsTrk


constexpr bool ActsTrk::MutableMultiTrajectory::hasColumn_impl(
    Acts::HashedString key) const {
  using namespace Acts::HashedStringLiteral;
  switch (key) {
    case "previous"_hash:
    case "chi2"_hash:
    case "pathLength"_hash:
    case "typeFlags"_hash:
    case "predicted"_hash:
    case "filtered"_hash:
    case "smoothed"_hash:
    case "jacobian"_hash:
    case "projector"_hash:
    case "uncalibratedSourceLink"_hash:
    case "calibrated"_hash:
    case "calibratedCov"_hash:
    case "measdim"_hash:
      return true;
    default:
      for (auto& d : m_decorations) {
        if (d.hash == key) {
          return true;
        }
      }
      return false;
  }
}

template <typename T>
void ActsTrk::MutableMultiTrajectory::addColumn_impl(const std::string& name) {
  // It is actually not clear if we would allow decorating RO MTJ, maybe we do
  using std::placeholders::_1;
  using std::placeholders::_2;
  if constexpr (ActsTrk::detail::accepted_decoration_types<T>::value) {
    m_decorations.emplace_back(
        name,
        std::bind(&ActsTrk::MutableMultiTrajectory::decorationSetter<T>, this,
                  _1, _2),
        std::bind(&ActsTrk::MutableMultiTrajectory::decorationGetter<T>, this,
                  _1, _2));
    // it would be useful to force presence of decoration already here
  } else {
    throw std::runtime_error("Can't add decoration of this type to MutableMultiTrajectory");
  }
}

template <typename T>
std::any ActsTrk::MutableMultiTrajectory::decorationSetter(
    IndexType idx, const std::string& name) {
  // TODO this approach is known not to be fastest possible, it would be better
  // to construct SG::Accessor and reuse it
  // however the SG::Accessor is typed and we'll need a classic inheritance for
  // that
  const SG::ConstAuxElement el(m_trackStates.get(), idx);  
  return &(el.auxdataConst<T>(name));

}

template <typename T>
const std::any ActsTrk::MutableMultiTrajectory::decorationGetter(
    ActsTrk::IndexType idx, const std::string& name) const {
  const auto& trackStates = *m_trackStates;
  const SG::AuxElement* el = trackStates[idx];
  return &(el->auxdecor<T>(name));
}

template <typename T>
const std::any ActsTrk::MultiTrajectory::decorationGetter(
    ActsTrk::IndexType idx, const std::string& name) const {
  const auto& trackStates = *m_trackStates;
  const SG::AuxElement* el = trackStates[idx];
  return static_cast<const T*>(&(el->auxdecor<T>(name)));
}
